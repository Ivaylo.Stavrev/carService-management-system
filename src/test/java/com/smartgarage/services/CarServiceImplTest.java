package com.smartgarage.services;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.repositories.contracts.CarRepository;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.VisitService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CarServiceImplTest {

    @Mock
    CarRepository mockCarRepository;

    @Mock
    BrandService mockBrandService;

    @Mock
    ModelService mockModelService;

    @Mock
    VisitService mockVisitService;

    @InjectMocks
    CarServiceImpl service;

    @Test
    public void getAll_should_Throw_when_UserIsNotEmployee() {
        // Arrange
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }

    @Test
    public void getAll_should_call_repository_when_UserIsEmployee() {
        // Arrange
        User mockEmployee = createMockEmployee();

        // Act
        service.getAll(mockEmployee);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_Throw_when_UserIsNotEmployee() {
        // Arrange
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(mockUser, createMockCar().getCarId()));
    }

    @Test
    public void getById_should_call_repository_when_UserIsEmployee() {
        // Arrange
        User mockEmployee = createMockEmployee();
        Car mockCar = createMockCar();

        // Act
        service.getById(mockEmployee, mockCar.getCarId());

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .getById(mockCar.getCarId());
    }

    @Test
    public void getByField_should_Throw_when_UserIsNotEmployee() {
        // Arrange
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByField(mockUser, "id", createMockCar().getCarId()));
    }

    @Test
    public void getByField_should_call_repository_when_UserIsEmployee() {
        // Arrange
        User mockEmployee = createMockEmployee();
        Car mockCar = createMockCar();

        // Act
        service.getByField(mockEmployee, "id", mockCar.getCarId());

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .getByField("id", mockCar.getCarId());
    }

    @Test
    public void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockCarRepository.filter(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .filter(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty());
    }

    @Test
    public void create_should_throw_when_performerIsNotOwnerOrEmployee() {
        // Arrange
        User mockUser = createMockUser();
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockUser, mockCar));
    }

    @Test
    public void create_should_call_repository_when_performerIsOwner() {
        // Arrange
        User mockUser = createMockUser();
        Car mockCar = createMockCar();
        Brand mockBrand = createMockBrand();
        CarModel mockModel = createMockCarModel();

        Mockito.when(mockBrandService.getByBrandName(mockBrand.getName()))
                .thenReturn(mockBrand);

        Mockito.when(mockModelService.getByModelName(mockModel.getName()))
                .thenReturn(mockModel);

        // Act
        service.create(mockUser, mockCar);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .create(mockCar);
    }

    @Test
    public void create_should_call_repository_when_performerIsEmployee() {
        // Arrange
        User mockUser = createMockEmployee();
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1);
        Brand mockBrand = createMockBrand();
        CarModel mockModel = createMockCarModel();

        Mockito.when(mockBrandService.getByBrandName(mockBrand.getName()))
                .thenReturn(mockBrand);

        Mockito.when(mockModelService.getByModelName(mockModel.getName()))
                .thenReturn(mockModel);

        // Act
        service.create(mockUser, mockCar);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .create(mockCar);
    }

    @Test
    public void update_should_throw_when_performerIsNotOwnerOrEmployee() {
        // Arrange
        User mockUser = createMockUser(); // not employee
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1); // not the same user

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockUser, mockCar));
    }

    @Test
    public void update_should_call_repository_when_UserIsOwner() {
        // Arrange
        User mockUser = createMockUser(); // not employee
        Car mockCar = createMockCar();
        mockCar.setUser(mockUser);      // user is Owner

        // Act
        service.update(mockUser, mockCar);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .update(mockCar);
    }

    @Test
    public void update_should_call_repository_when_UserIsEmployee() {
        // Arrange
        User mockUser = createMockEmployee(); // is employee
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1); // not Owner

        // Act
        service.update(mockUser, mockCar);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .update(mockCar);
    }

    @Test
    public void delete_should_throw_when_performerIsNotOwnerOrEmployee() {
        // Arrange
        User mockUser = createMockUser(); // not employee
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1); // not the same user

        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCar);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser, mockCar.getCarId()));
    }

    @Test
    public void delete_should_call_repository_when_UserIsOwner() {
        // Arrange
        User mockUser = createMockUser(); // not employee
        Car mockCar = createMockCar();
        mockCar.setUser(mockUser);      // user is Owner
        List<Visit> mockVisits = new ArrayList<>();

        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCar);

        Mockito.when(mockVisitService.getVisitsByCarId(mockCar.getCarId(), mockUser))
                .thenReturn(mockVisits);

        // Act
        service.delete(mockUser, mockCar.getCarId());

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .delete(mockCar);
    }

    @Test
    public void delete_should_call_repository_when_UserIsEmployee() {
        // Arrange
        User mockUser = createMockEmployee(); // is employee
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1); // not the same user
        List<Visit> mockVisits = new ArrayList<>();

        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCar);

        Mockito.when(mockVisitService.getVisitsByCarId(mockCar.getCarId(), mockUser))
                .thenReturn(mockVisits);

        // Act
        service.delete(mockUser, mockCar.getCarId());

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .delete(mockCar);
    }

    @Test
    public void delete_should_throw_when_ListOfVisitsIsNotEmpty() {
        // Arrange
        User mockUser = createMockEmployee(); // is employee
        Car mockCar = createMockCar();
        mockCar.getUser().setUserId(mockUser.getUserId() + 1); // not the same user
        List<Visit> mockVisits = new ArrayList<>();
        mockVisits.add(createMockVisit());

        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCar);

        Mockito.when(mockVisitService.getVisitsByCarId(mockCar.getCarId(), mockUser))
                .thenReturn(mockVisits);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser, mockCar.getCarId()));
    }

    @Test
    public void delete_should_throw_when_CarDoesNotExists() {
        // Arrange
        User mockUser = createMockUser();
        Car mockCar = createMockCar();

        Mockito.when(mockCarRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.delete(mockUser, mockCar.getCarId()));
    }

}
