package com.smartgarage.services;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.CarServiceRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.Helpers.*;


@ExtendWith(MockitoExtension.class)
class CarServiceServiceImplTest {

    @Mock
    CarServiceRepository mockServiceRepository;

    @Mock
    VisitRepository mockVisitRepository;

    @InjectMocks
    CarServiceServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockServiceRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void filter_should_callRepository() {
        // Arrange
        Mockito.when(mockServiceRepository.filter(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .filter(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty());
    }

    @Test
    public void getById_should_returnService_when_matchExist() {
        // Arrange
        CarService mockService = createMockCarService();
        Mockito.when(mockServiceRepository.getById(mockService.getCarServiceId()))
                .thenReturn(mockService);
        // Act
        CarService result = service.getById(createMockEmployee(), mockService.getCarServiceId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockService.getCarServiceId(), result.getCarServiceId()),
                () -> Assertions.assertEquals(mockService.getName(), result.getName()),
                () -> Assertions.assertEquals(mockService.getPrice(), result.getPrice()),
                () -> Assertions.assertEquals(mockService.getServiceStatus(), result.getServiceStatus())
        );
    }

    @Test
    public void create_should_throw_when_serviceWithSameNameExists() {
        // Arrange
        CarService mockCarService = createMockCarService();

        Mockito.when(mockServiceRepository.getByName(mockCarService.getName()))
                .thenReturn(mockCarService);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(createMockEmployee(), mockCarService));
    }

    @Test
    public void create_should_callRepository_when_serviceWithSameNameDoesNotExist() {
        // Arrange
        CarService mockService = createMockCarService();

        Mockito.when(mockServiceRepository.getByName(mockService.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(createMockEmployee(), mockService);

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .create(mockService);
    }

    @Test
    public void update_should_throwException_when_userIsNotEmployee() {
        // Arrange
        CarService mockService = createMockCarService();
        User performer = createMockUser(); // regular user

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(performer, mockService));
    }


    @Test
    public void update_should_callRepository_when_userIsEmployee() {
        // Arrange
        CarService mockService = createMockCarService();
        User performer = createMockEmployee();

        Mockito.when(mockServiceRepository.getById(Mockito.anyInt()))
                .thenReturn(mockService);

        Mockito.when(mockServiceRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.update(performer, mockService);

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .update(mockService);
    }

    @Test
    public void update_should_throwException_when_serviceNameIsTaken() {
        // Arrange
        CarService mockService = createMockCarService();
        mockService.setName("test-name");
        CarService anotherMockService = createMockCarService();
        anotherMockService.setCarServiceId(2);
        anotherMockService.setName("test-name");

        Mockito.when(mockServiceRepository.getById(Mockito.anyInt()))
                .thenReturn(mockService);

        Mockito.when(mockServiceRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockService);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(createMockEmployee(), mockService));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingService() {
        // Arrange
        CarService mockService = createMockCarService();

        Mockito.when(mockServiceRepository.getById(Mockito.anyInt()))
                .thenReturn(mockService);

        Mockito.when(mockServiceRepository.getByName(Mockito.anyString()))
                .thenReturn(mockService);


        // Act
        service.update(createMockEmployee(), mockService);

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .update(mockService);
    }

    @Test
    public void delete_should_throwException_when_performerIsNotEmployee() {
        // Arrange
        CarService mockService = createMockCarService();
        User mockPerformer = createMockUser();
        mockPerformer.setUserName("MockPerformer");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockPerformer, mockService.getCarServiceId()));
    }

    @Test
    public void delete_should_throwException_when_performerIsEmployeeButListOfVisitsIsNotEmpty() {
        // Arrange
        CarService mockService = createMockCarService();
        User mockPerformer = createMockEmployee();
        mockPerformer.setUserName("MockPerformer");

        Mockito.when(mockServiceRepository.getById(mockService.getCarServiceId()))
                .thenReturn(mockService);

        Mockito.when(mockVisitRepository.getVisitsByCarServiceId(mockService.getCarServiceId()))
                .thenReturn(List.of(createMockVisit()));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockPerformer, mockService.getCarServiceId()));
    }

    @Test
    public void delete_should_callRepository_when_performerIsEmployeeAndVisitsIsEmpty() {
        // Arrange
        CarService mockService = createMockCarService();
        User mockPerformer = createMockEmployee();
        mockPerformer.setUserName("MockPerformer");

        Mockito.when(mockServiceRepository.getById(mockService.getCarServiceId()))
                .thenReturn(mockService);

        Mockito.when(mockVisitRepository.getVisitsByCarServiceId(mockService.getCarServiceId()))
                .thenReturn(List.of());

        // Act
        service.delete(mockPerformer, mockService.getCarServiceId());

        // Assert
        Mockito.verify(mockServiceRepository, Mockito.times(1))
                .delete(mockService);
    }

}
