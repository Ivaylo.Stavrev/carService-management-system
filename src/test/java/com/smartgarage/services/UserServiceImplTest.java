package com.smartgarage.services;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.CarServiceRepository;
import com.smartgarage.repositories.contracts.UserRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.Helpers.*;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_callRepositoryWhenPerformerIsEmployee() {
        // Arrange
        User mockCustomer = createMockUser();
        User mockEmployee = createMockEmployee();
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(List.of(mockCustomer));

        // Act
        service.getAll(mockEmployee);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throwExceptionWhenPerformerIsNotEmployee() {
        // Arrange
        User mockUser = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }

    @Test
    public void getById_should_callRepositoryWhenPerformerIsEmployee() {
        // Arrange
        User mockCustomer = createMockUser();
        User mockEmployee = createMockEmployee();
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCustomer);

        // Act
        User result = service.getById(mockEmployee, mockCustomer.getUserId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockCustomer.getUserName(), result.getUserName()),
                () -> Assertions.assertEquals(mockCustomer.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockCustomer.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone())
        );
    }

    @Test
    public void getById_should_callRepositoryWhenPerformerIsSameUser() {
        // Arrange
        User mockCustomer = createMockUser();
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCustomer);

        // Act
        User result = service.getById(mockCustomer, mockCustomer.getUserId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCustomer.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockCustomer.getUserName(), result.getUserName()),
                () -> Assertions.assertEquals(mockCustomer.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockCustomer.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockCustomer.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockCustomer.getPhone(), result.getPhone())
        );
    }

    @Test
    public void getById_should_throwExceptionWhenPerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        User performer = createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(performer, performer.getUserId() + 1));
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        // Arrange
        User mockPerformer = createMockEmployee();
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getById(mockUser.getUserId()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockPerformer, mockUser.getUserId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(mockUser.getUserName(), result.getUserName()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhone(), result.getPhone())
        );
    }

    @Test
    public void getById_should_throwException_when_matchDoesNotExist() {
        // Arrange
        User mockPerformer = createMockEmployee();
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(mockPerformer, Mockito.anyInt()));

    }

    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_throw_when_userWithSamePhoneExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_callRepository_when_userWithSameUsernameOrEmailOrPhoneDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void update_should_throwExceptionWhenPerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        User performer = createMockUser();
        User target = createMockUser();
        target.setUserId(performer.getUserId()+1);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(target, performer));
    }

    @Test
    public void update_should_throw_when_userWithSameUsernameExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser, createMockEmployee()));
    }

    @Test
    public void update_should_throw_when_userWithSameEmailExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser, createMockEmployee()));
    }

    @Test
    public void update_should_throw_when_userWithSamePhoneExists() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenReturn(mockUser);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(mockUser, createMockEmployee()));
    }

    @Test
    public void update_should_callRepository_when_userWithSameUsernameOrEmailOrPhoneDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField("userName", mockUser.getUserName()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByField("phone", mockUser.getPhone()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.update(mockUser, createMockEmployee());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void delete_should_throwExceptionWhenPerformerIsNotEmployeeAndNotSameUser() {
        // Arrange
        User performer = createMockUser();
        User target = createMockUser();
        target.setUserId(performer.getUserId()+1);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(performer, target.getUserId()));
    }

    @Test
    public void delete_should_callRepository_when_targetIsSameUserOrEmployee() {
        // Arrange
        User mockCustomer = createMockUser();
        User mockEmployee = createMockEmployee();
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCustomer);

        // Act
        service.delete(mockEmployee, mockCustomer.getUserId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .delete(mockCustomer);
    }

}
