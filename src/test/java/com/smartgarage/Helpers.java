package com.smartgarage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartgarage.models.*;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.enums.Currency;
import com.smartgarage.models.enums.ServiceStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    public static User createMockUser() {
        return createMockUser("Customer", 1);
    }

    public static User createMockEmployee() {
        return createMockUser("Employee", 2);
    }

    private static User createMockUser(String role, int roleId) {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setUserName("MockUsername");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setPassword("MockPassword");
        mockUser.setPhone("MockPhone");
        mockUser.setCurrency(Currency.BGN);
        mockUser.setEnabled(false);
        mockUser.setRoles(getRoles(role, roleId));
        mockUser.setVisits(getVisits());
        return mockUser;
    }

    public static Role createMockRole(String role, int roleId) {
        var mockRole = new Role();
        mockRole.setRoleId(roleId);
        mockRole.setRole(role);
        return mockRole;
    }

    public static Car createMockCar() {
        var mockCar = new Car();
        mockCar.setCarId(1);
        mockCar.setLicensePlate("MockLicensePlate");
        mockCar.setVin("MockVIN");
        mockCar.setYear(2000);
        mockCar.setBrand(createMockBrand());
        mockCar.setCarModel(createMockCarModel());
        mockCar.setUser(createMockUser());
        return mockCar;
    }

    public static Brand createMockBrand() {
        var mockBrand = new Brand();
        mockBrand.setBrandId(1);
        mockBrand.setName("MockBrandName");
        mockBrand.setActive(true);
        mockBrand.setModels(getModels());
        return mockBrand;
    }

    public static CarModel createMockCarModel() {
        var mockModel = new CarModel();
        mockModel.setModelId(1);
        mockModel.setName("MockModelName");
        mockModel.setActive(true);
        return mockModel;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();
        mockVisit.setVisitId(1);
        mockVisit.setCheckIn(LocalDateTime.now().minusDays(3));
        mockVisit.setCheckOut(LocalDateTime.now().minusDays(1));
        mockVisit.setServices(getServices());
        return mockVisit;
    }

    public static CarService createMockCarService() {
        var mockService = new CarService();
        mockService.setCarServiceId(1);
        mockService.setName("MockCarServiceName");
        mockService.setPrice(99);
        mockService.setServiceStatus(ServiceStatus.ACTIVE);
        return mockService;
    }

    private static Set<Visit> getVisits() {
        Set<Visit> visits = new HashSet<>();
        visits.add(createMockVisit());
        return visits;
    }

    private static List<CarService> getServices() {
        List<CarService> services = new ArrayList<>();
        services.add(createMockCarService());
        return services;
    }

    private static Set<Role> getRoles(String role, int roleId) {
        Set<Role> roles = new HashSet<>();
        roles.add(createMockRole(role, roleId));
        return roles;
    }

    private static List<CarModel> getModels() {
        List<CarModel> models = new ArrayList<>();
        models.add(createMockCarModel());
        return models;
    }

    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
