package com.smartgarage.models.attributes;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "models")
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private int modelId;

    @Column(name = "name")
    private String name;

    @Column(name = "is_active")
    private boolean isActive = true;

    public CarModel() {
    }

    public CarModel(String name) {
        this.name = name;
    }

    // AllArgsConstructor for testing purposes - to be removed before deploy - I.Stavrev
    public CarModel(int modelId, String name) {
        this.modelId = modelId;
        this.name = name;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarModel carModel = (CarModel) o;
        return Objects.equals(name, carModel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
