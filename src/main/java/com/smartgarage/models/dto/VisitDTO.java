package com.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

public class VisitDTO {

    @NotNull
    @Size(min = 16, max = 16, message = "Valid time is with 16 symbols.")
    private String checkInDate;

    @NotNull
    @Size(min = 16, max = 16, message = "Valid time is with 16 symbols.")
    private String checkOutDate;

    @Positive
    private int carId;

    @Positive
    private int userId;

    private List<Integer> servicesIds;

    public VisitDTO() {
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Integer> getServicesIds() {
        return servicesIds;
    }

    public void setServicesIds(List<Integer> servicesIds) {
        this.servicesIds = servicesIds;
    }
}
