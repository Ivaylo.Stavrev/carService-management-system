package com.smartgarage.models.dto;

public class CarOutDTO {

    private Integer carId;
    private String licensePlate;
    private String vin;
    private Integer year;
    private String brandName;
    private String modelName;
    private UserOutDTO owner;

    public CarOutDTO() {
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public UserOutDTO getOwner() {
        return owner;
    }

    public void setOwner(UserOutDTO owner) {
        this.owner = owner;
    }
}
