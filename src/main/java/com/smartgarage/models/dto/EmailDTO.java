package com.smartgarage.models.dto;

import com.smartgarage.utils.validator.contracts.ValidEmail;

import javax.validation.constraints.NotNull;

public class EmailDTO {

    @NotNull
    @ValidEmail
    private String email;

    public EmailDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
