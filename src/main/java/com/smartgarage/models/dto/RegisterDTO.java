package com.smartgarage.models.dto;

import com.smartgarage.utils.validator.contracts.PasswordMatches;
import com.smartgarage.utils.validator.contracts.ValidEmail;
import com.smartgarage.utils.validator.contracts.ValidPassword;

import javax.validation.constraints.*;

@PasswordMatches
public class RegisterDTO {

    @NotNull
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String userName;

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    @NotNull
    @ValidEmail
    private String email;

    @NotNull
    @ValidPassword
    private String password;

    @NotNull
    private String matchingPassword;

    @NotNull
    @Size(min = 10, max = 10, message = "Phone should be at least 10 symbols")
    private String phone;

    private String token;

    public RegisterDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
