package com.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CarDTO {

    @NotNull
    @Size(min = 4, max = 12, message = "Licence plate error")
    private String licensePlate;

    @NotNull
    @Size(min = 17, max = 17, message = "VIN should be exactly 17 symbols")
    private String vin;

    @NotNull
    @Size(min = 4, max = 4, message = "Year should be exactly 4 symbols")
    private String year;

    @NotNull
    private String brandName;

    @NotNull
    private String modelName;

    @NotNull
    private String userName;

    public CarDTO() {
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        licensePlate = licensePlate.toUpperCase();
        String bulStr = "АВСЕНКМОРТХУ"; // cyrillic letters
        String[] bulArr = bulStr.split("");
        String engStr = "ABCEHKMOPTXY"; // latin letters
        String[] engArr = engStr.split("");
        String[] lp = licensePlate.split("");
        for (int i = 0; i < lp.length; i++) { // У
            if (lp[i].charAt(0) > 47 && lp[i].charAt(0) < 58) continue;
            if (lp[i].charAt(0) > 64 && lp[i].charAt(0) < 91) continue;
            for (int j = 0; j < bulArr.length; j++) {
                if (bulArr[j].equals(lp[i])) {
                    lp[i] = engArr[j];
                    break;
                }
                if (j == bulArr.length - 1) {
                    lp[i] = "?";
                }
            }
        }
        licensePlate = String.join("", lp);
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
