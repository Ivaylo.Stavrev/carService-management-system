package com.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BrandDTO {

    @NotNull
    @Size(min = 2, max = 32, message = "Brand name should be between 2 and 32 symbols")
    private String brandName;

    public BrandDTO() {
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
