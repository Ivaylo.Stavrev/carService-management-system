package com.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BrandAndModelDTO {

    @NotNull
    @Size(min = 2, max = 32, message = "Brand name should be between 2 and 32 symbols")
    private String brandName;

    @NotNull
    @Size(min = 2, max = 32, message = "Model name should be between 2 and 32 symbols")
    private String modelName;

    public BrandAndModelDTO() {
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
