package com.smartgarage.models;

import com.smartgarage.models.enums.ServiceStatus;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "services")
public class CarService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int carServiceId;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ServiceStatus serviceStatus;

    public CarService() {
        setServiceStatus(ServiceStatus.ACTIVE);
    }

    public int getCarServiceId() {
        return carServiceId;
    }

    public void setCarServiceId(int serviceId) {
        this.carServiceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public boolean isActive(){
        return getServiceStatus().name().equalsIgnoreCase("ACTIVE");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarService carService = (CarService) o;
        return Objects.equals(name, carService.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
