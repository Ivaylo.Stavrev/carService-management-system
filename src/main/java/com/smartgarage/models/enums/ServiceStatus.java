package com.smartgarage.models.enums;

public enum ServiceStatus {
    ACTIVE,INACTIVE;

    @Override
    public String toString() {
        switch (this){
            case ACTIVE:
                System.out.println("ACTIVE");
            case INACTIVE:
                System.out.println("INACTIVE");
            default:
                throw new IllegalArgumentException("Enum not found");
        }
    }
}
