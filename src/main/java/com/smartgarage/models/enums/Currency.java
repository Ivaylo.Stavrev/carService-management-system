package com.smartgarage.models.enums;

public enum Currency {
    BGN, EUR, USD
}
