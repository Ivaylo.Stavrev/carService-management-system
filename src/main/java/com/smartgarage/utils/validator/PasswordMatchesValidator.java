package com.smartgarage.utils.validator;

import com.smartgarage.models.dto.RegisterDTO;
import com.smartgarage.utils.validator.contracts.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        RegisterDTO user = (RegisterDTO) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}