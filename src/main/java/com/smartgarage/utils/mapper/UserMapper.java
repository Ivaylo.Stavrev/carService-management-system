package com.smartgarage.utils.mapper;

import com.github.javafaker.Faker;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.models.Role;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.CustomerDTO;
import com.smartgarage.models.dto.RegisterDTO;
import com.smartgarage.models.dto.UserDTO;
import com.smartgarage.models.dto.UserOutDTO;
import com.smartgarage.services.contracts.UserService;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserMapper {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public User registerDtoToUserCreate(RegisterDTO registerDTO) {
        User user = new User();
        user.setUserName(registerDTO.getUserName());
        user.setFirstName(registerDTO.getFirstName());
        user.setLastName(registerDTO.getLastName());
        user.setEmail(registerDTO.getEmail());
        user.setJoinDate(new Date());
        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
        user.setPhone(registerDTO.getPhone());

        Role role = new Role();
        role.setRoleId(2);
        role.setRole("Employee");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);

        return user;
    }

    public User customerDtoToUserCreate(CustomerDTO customerDTO) {
        Faker faker = new Faker();
        String userName = faker.hipster().word();
        while (userService.userExists("userName", userName)){
            userName = faker.hipster().word();
            if (!userService.userExists("userName", userName)) break;
        }
        String firstName = faker.backToTheFuture().character();
        String lastName = faker.hobbit().character();
        String randomPass = generatePassayPassword();

        User user = new User();
        user.setUserName(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(customerDTO.getEmail());
        user.setJoinDate(new Date());
        user.setPassword(passwordEncoder.encode(randomPass));
        user.setPhone(customerDTO.getPhone());

        Role role = new Role();
        role.setRoleId(1);
        role.setRole("Customer");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);

        return user;
    }

    public User registerDtoToUserUpdate(RegisterDTO registerDTO, User user) {
        user.setUserName(registerDTO.getUserName());
        user.setFirstName(registerDTO.getFirstName());
        user.setLastName(registerDTO.getLastName());
        user.setEmail(registerDTO.getEmail());
        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
        user.setPhone(registerDTO.getPhone());
        return user;
    }

    public RegisterDTO userToRegisterDTO(User user, String token) {
        RegisterDTO registerDTO = new RegisterDTO();
        registerDTO.setUserName(user.getUserName());
        registerDTO.setEmail(user.getEmail());
        registerDTO.setPhone(user.getPhone());

        registerDTO.setToken(token);
        return registerDTO;
    }

    public UserOutDTO userToUserOutDto(User user) {
        UserOutDTO userOutDTO = new UserOutDTO();
        userOutDTO.setUserId(user.getUserId());
        userOutDTO.setUsername(user.getUserName());
        userOutDTO.setFirstName(user.getFirstName());
        userOutDTO.setLastName(user.getLastName());
        userOutDTO.setEmail(user.getEmail());
        userOutDTO.setPhone(user.getPhone());
        return userOutDTO;
    }

    public String generatePassayPassword() {
        PasswordGenerator gen = new PasswordGenerator();
        List<CharacterRule> rules = new ArrayList<>();

        rules.add(new UppercaseCharacterRule(1));
        rules.add(new DigitCharacterRule(1));
        rules.add(new SpecialCharacterRule(1));

        return gen.generatePassword(8, rules);
    }

    public User userDtoToUserUpdate(User loggedInUser, UserDTO userDTO, User userToBeUpdated) {
        userToBeUpdated.setUserName(userDTO.getUserName());
        userToBeUpdated.setFirstName(userDTO.getFirstName());
        userToBeUpdated.setLastName(userDTO.getLastName());
        userToBeUpdated.setEmail(userDTO.getEmail());
        userToBeUpdated.setPhone(userDTO.getPhone());
        return userToBeUpdated;
    }
}
