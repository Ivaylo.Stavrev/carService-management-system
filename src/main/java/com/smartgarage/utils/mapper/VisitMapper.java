package com.smartgarage.utils.mapper;

import com.smartgarage.models.Car;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dto.VisitDTO;
import com.smartgarage.services.contracts.CarServiceService;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class VisitMapper {

    private final com.smartgarage.services.contracts.CarService carService;

    private final UserService userService;

    private final CarServiceService carServiceService;

    @Autowired
    public VisitMapper(com.smartgarage.services.contracts.CarService carService, UserService userService, CarServiceService carServiceService) {
        this.carService = carService;
        this.userService = userService;
        this.carServiceService = carServiceService;
    }

    public Visit createVisitFromDTO(User loggedInUser, VisitDTO visitDTO) {
        List<CarService> carServices = new ArrayList<>();
        Car car = carService.getById(loggedInUser, visitDTO.getCarId());
        User user = userService.getById(loggedInUser, visitDTO.getUserId());
        visitDTO.getServicesIds().forEach(serviceId -> carServices.add(carServiceService.getById(loggedInUser, serviceId)));

        Visit newVisit = new Visit();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        newVisit.setCheckIn(LocalDateTime.parse(visitDTO.getCheckInDate(), formatter));
        newVisit.setCheckOut(LocalDateTime.parse(visitDTO.getCheckOutDate(), formatter));
        newVisit.setCar(car);
        newVisit.setUser(user);
        newVisit.setServices(carServices);
        return newVisit;
    }

}
