package com.smartgarage.repositories;

import com.smartgarage.models.Car;
import com.smartgarage.models.CarService;
import com.smartgarage.models.enums.CarSortOptions;
import com.smartgarage.repositories.contracts.CarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Repository
public class CarRepositoryImpl extends AbstractCRUDRepository<Car> implements CarRepository {

    private final SessionFactory sessionFactory;

    public CarRepositoryImpl(SessionFactory sessionFactory) {
        super(Car.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Car> search(String search, List<String> parameters) {
        StringBuilder qeuryBuild = new StringBuilder("select c from %s c join c.user u where ");

        String paramsJoined = parameters.stream()
                .map(s -> String.format(" %s like :value ", s))
                .collect(Collectors.joining(" or "));

        qeuryBuild.append(paramsJoined);

        final String query = format(qeuryBuild.toString(), "Car");
        try (Session session = sessionFactory.openSession()){
            return session.createQuery(query, Car.class)
                    .setParameter("value", "%" + search + "%")
                    .list();
        }
    }

    // Filter and sort vehicles by model, brand, year of creation (must)
    @Override
    public List<Car> filter(Optional<String> brandName,
                                   Optional<String> modelName,
                                   Optional<Integer> year,
                                   Optional<CarSortOptions> sortOptions) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("select c from Car c join c.brand b join c.carModel m ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            brandName.ifPresent(value -> {
                filters.add(" b.name like :brandName");
                params.put("brandName", value);
            });

            modelName.ifPresent(value -> {
                filters.add(" m.name like :modelName");
                params.put("modelName", "%" + value + "%");
            });

            year.ifPresent(value -> {
                filters.add(" c.year = :year");
                params.put("year", value);
            });

            if (!filters.isEmpty()) {
                baseQuery.append("where ").append(String.join(" and ", filters));
            }

            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));

            return session.createQuery(baseQuery.toString(), Car.class)
                    .setProperties(params)
                    .list();
        }
    }

}
