package com.smartgarage.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T>{

    void delete(T entity);

    void create(T entity);

    void update(T entity);
}
