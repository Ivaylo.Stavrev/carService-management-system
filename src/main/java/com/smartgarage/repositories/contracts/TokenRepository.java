package com.smartgarage.repositories.contracts;

import com.smartgarage.models.tokens.Token;

public interface TokenRepository extends BaseCRUDRepository<Token> {
}
