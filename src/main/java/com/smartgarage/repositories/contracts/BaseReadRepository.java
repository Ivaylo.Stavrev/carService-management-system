package com.smartgarage.repositories.contracts;

import java.util.List;

public interface BaseReadRepository<T> {

    T getByName(String name);

    List<T> getAll();

    T getById(int id);

    <V> T getByField(String name, V value);

}
