package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Car;
import com.smartgarage.models.CarService;
import com.smartgarage.models.enums.CarSortOptions;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends BaseCRUDRepository<Car>{

    List<Car> search(String search, List<String> parameters);

    // Filter and sort vehicles by model, brand, year of creation (must)
    List<Car> filter(Optional<String> modelName,
                            Optional<String> brandName,
                            Optional<Integer> year,
                            Optional<CarSortOptions> sortOptions);
}
