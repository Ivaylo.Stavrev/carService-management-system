package com.smartgarage.repositories.contracts;

import com.smartgarage.models.attributes.CarModel;

import java.util.List;

public interface ModelRepository extends BaseCRUDRepository<CarModel>{
    List<CarModel> getAllModels();

    List<String> getModelNamesByBrandName(String brandName);

    List<String> getModelsNotInUse();
}
