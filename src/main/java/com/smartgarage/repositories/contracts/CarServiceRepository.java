package com.smartgarage.repositories.contracts;

import com.smartgarage.models.CarService;
import com.smartgarage.models.enums.ServiceSortOptions;

import java.util.List;
import java.util.Optional;

public interface CarServiceRepository extends BaseCRUDRepository<CarService>{

    List<CarService> getServicesByCarId(int carId);

    List<CarService> filter(Optional<String> name,
                            Optional<Double> minPrice,
                            Optional<Double> maxPrice,
                            Optional<ServiceSortOptions> sortOptions);
}
