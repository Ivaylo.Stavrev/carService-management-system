package com.smartgarage.repositories;

import com.smartgarage.models.tokens.PasswordToken;
import com.smartgarage.repositories.contracts.PasswordTokenRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PasswordTokenRepositoryImpl extends AbstractCRUDRepository<PasswordToken> implements PasswordTokenRepository {
    public PasswordTokenRepositoryImpl(SessionFactory sessionFactory) {
        super(PasswordToken.class, sessionFactory);
    }
}
