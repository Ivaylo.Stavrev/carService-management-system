package com.smartgarage.repositories;

import com.smartgarage.models.attributes.Brand;
import com.smartgarage.repositories.contracts.BrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BrandRepositoryImpl extends AbstractCRUDRepository<Brand> implements BrandRepository {

    private final SessionFactory sessionFactory;

    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        super(Brand.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brand> getAllBrands() {
        try (Session session = sessionFactory.openSession()){
            String query = "from Brand where isActive = true order by name";
            return session.createQuery(query, Brand.class).list();
        }
    }

    @Override
    public void removeModelsByBrandName(String brandName) {
        try (Session session = sessionFactory.openSession()){
            String query = "update CarModel m set m.isActive = false " +
                    "where m.id in (select m.id from Brand b join b.models m where b.name = :value)";
            session.beginTransaction();
            session.createQuery(query)
                    .setParameter("value", brandName)
                    .executeUpdate();
            session.getTransaction().commit();
        }
    }

}
