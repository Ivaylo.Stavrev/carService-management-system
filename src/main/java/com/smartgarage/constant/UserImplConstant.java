package com.smartgarage.constant;

import java.text.DecimalFormat;

public class UserImplConstant {

    public static final String CUSTOMER = "customer";

    public static final String NO_USER_FOUND_BY_USERNAME = "No user found by username: ";
    public static final String FOUND_USER_BY_USERNAME = "Returning found user by username: ";
    public static final String NOT_OWNER = "Only employees and owners are allowed to modify user profile!";
    public static final String NOT_EMPLOYEE = "Only employees are allowed to enable user profile!";
    public static final String NOT_EMPLOYEE_CREATE_SERVICE = "Only employees are allowed to create new service!";
    public static final String NOT_EMPLOYEE_UPDATE_SERVICE = "Only employees are allowed to edit new service!";
    public static final String NOT_EMPLOYEE_DELETE_SERVICE = "Only employees are allowed to delete service!";

    public static final String SERVICE_WITH_VISITS_DELETE_ERROR = "Only services with no visits can be deleted!";
    public static final String CAR_WITH_VISITS_DELETE_ERROR = "Only cars with no visits can be deleted!";
    public static final String USER_IS_DISABLED = "User is disabled. Please contact an employee.";

    public static final DecimalFormat doubleTwoDecimalPlaces = new DecimalFormat("0.00");
    public static final String DEFAULT_CURRENCY = "BGN";

    public static final String FIRST_NAME = "u.firstName";
    public static final String LAST_NAME = "u.lastName";
    public static final String EMAIL = "u.email";
    public static final String PHONE = "u.phone";
    public static final String MODEL = "c.carModel.name";
    public static final String BRAND = "c.brand.name";
}
