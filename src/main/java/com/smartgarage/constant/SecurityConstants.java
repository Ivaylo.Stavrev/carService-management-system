package com.smartgarage.constant;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 432_000_000; // 5 days expressed in milliseconds
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Token cannot be verified";
    public static final String THE_FATHERS = "The Fathers";
    public static final String THE_FATHERS_ADMINISTRATION = "User Management Portal";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "You need to log in to access this page";
    public static final String ACCESS_DENIED_MESSAGE = "You do not have permission to access this page";
    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = {
            "/",
            "/employee/register/confirm",
            "/customer/register/confirm",
            "/password/forget",
            "/password/reset",
            "/bad-user",
            "/fontawesome*",
            "/**/*.png",
            "/**/*.jpg",
            "/**/*.svg",
            "/**/*.ico",
            "/**/*.json",
            "/**/*.scss",
            "/**/*.js",
            "/**/*.css"
    };
}
