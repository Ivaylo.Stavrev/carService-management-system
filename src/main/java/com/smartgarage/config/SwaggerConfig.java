package com.smartgarage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Open on -> http://localhost:8080/swagger-ui/
     */

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.smartgarage"))
                .paths(regex("/api.*"))
                .build()
                .apiInfo(apiDetails())
                .securityContexts(List.of(securityContext()))
                .securitySchemes(List.of(apiKey()));
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext
                .builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("JWT", authorizationScopes));
    }

    private ApiInfo apiDetails() {
        return new ApiInfoBuilder()
                .title("The Smartest Garage")
                .description("  Smart Garage is smart application for car-service management.\n\n" +
                        "   1. Users\n" +
                        "      •\tCRUD operations (*must*)\n" +
                        "      •\tEmployees must be able to search a user by username, email, phone, or car’s license plate/VIN (*must*)\n" +
                        "\n" +
                        "   2. Vehicles\n" +
                        "      •\tCRUD Operations (*must*)\n" +
                        "      •\tSearch a vehicle by license plate or VIN (*must*)\n" +
                        "      •\tEmployee must be able to search vehicles by owner’s phone number (*must*)\n" +
                        "      •\tFilter and sort vehicles by model, brand, year of creation (*must*)\n" +
                        "\n" +
                        "   3. Services\n" +
                        "      •\tCRUD operations (*must*)\n" +
                        "      •\tSearch by name (*must*)\n" +
                        "      •\tSearch by exact price (*must*)\n" +
                        "      •\tFilter and sort by name and price range. (*must*)\n")
                .termsOfServiceUrl("https://gitlab.com/TheFathers/cat-service-management-system")
                .license("Developed by: Chavdar Dimitrov and Ivaylo Stavrev")
                .licenseUrl("https://gitlab.com/TheFathers/cat-service-management-system")
                .version("0.0.1")
                .build();
    }

}
