package com.smartgarage.services.contracts;

import com.smartgarage.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll(User loggedInUser);

    List<User> getAllCustomers(User loggedInUser);

    List<User> getAll(Optional<String> search, User loggedInUser);

    User getById(User loggedInUser, int userId);

    <V> User getByField(String name, V value);

    void create(User newUser);

    void update(User target, User performer);

    void delete(User loggedInUser, int userId);

    void passwordReset(User user, String encryptedPassword);

    void updateUserPassword(User loggedInUser, int userId, String encryptedPassword);

    boolean userExists(String key, String value);

    String enable(User loggedInUser, int userId);

    String disable(User loggedInUser, int userId);
}
