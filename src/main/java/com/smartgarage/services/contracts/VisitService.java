package com.smartgarage.services.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.Visit;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface VisitService {

    List<Visit> getVisitsByCarId(int carId, User loggedInUser);

    List<Visit> getAll();

    List<Visit> getAll(Optional<String> search);

    Visit getById(int visitId, User loggedInUser);

    void create(Visit newVisit);

    void create(Visit newVisit, User performer);

    void update(Visit visit);

    void update(Visit visit, User performer);

    void delete(Visit visit);

    void delete(Visit visit, User performer);

    void createVisitForCarWithCarId(int carIdUser, User loggedInUser);

    void checkOut(int visitId, User loggedInUser);

    List<Visit> getVisitsByCarServiceId(int serviceId);

    double getExchangeRate(User loggedInUser, String currency, Visit visit);

    String createPdf(Visit visit, User loggedInUser, Optional<Double> exchangeRate, Optional<String> currency) throws IOException;
}
