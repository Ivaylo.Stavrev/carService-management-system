package com.smartgarage.services.contracts;

import com.smartgarage.models.attributes.CarModel;

import java.util.List;

public interface ModelService {

    void create(CarModel newCarModel);

    List<CarModel> getAllModels();

    List<String> getModelNamesByBrandName(String brandName);

    CarModel getByModelName(String name);

    List<String> getModelsNotInUse();

    void remove(String modelName);
}
