package com.smartgarage.services.contracts;

import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.enums.ServiceSortOptions;

import java.util.List;
import java.util.Optional;

public interface CarServiceService {

    List<CarService> getAll();

    List<CarService> filter(Optional<String> name,
                            Optional<Double> minPrice,
                            Optional<Double> maxPrice,
                            Optional<ServiceSortOptions> sortOptions);

    CarService getById(User loggedInUser, int serviceId);

    List<CarService> getServicesByCarId(int carId);

    void create(User loggedInUser, CarService newCarService);

    CarService getByName(String selectedService);

    List<Visit> getVisitsByCarServiceId(int serviceId);

    void update(User loggedInUser, CarService carService);

    void delete(User loggedInUser, int serviceId);
}
