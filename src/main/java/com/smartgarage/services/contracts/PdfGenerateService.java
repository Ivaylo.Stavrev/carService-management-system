package com.smartgarage.services.contracts;

import com.smartgarage.models.Visit;

import java.io.IOException;
import java.util.Optional;

public interface PdfGenerateService {
    String generatePdfFile(Visit visit, Optional<Double> exchangeRate, Optional<String> currency, String fileName) throws IOException;
}
