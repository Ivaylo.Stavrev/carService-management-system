package com.smartgarage.services;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.UserRepository;
import com.smartgarage.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.constant.UserImplConstant.*;

@Service
@Transactional
@Qualifier(("userDetailsService"))
public class UserServiceImpl implements UserService, UserDetailsService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll(User loggedInUser) {
        if (!loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        }
        return userRepository.getAll();
    }

    @Override
    public List<User> getAllCustomers(User loggedInUser) {
        if (!loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        }
        return userRepository.getAllCustomers();
    }

    @Override
    public List<User> getAll(Optional<String> search, User loggedInUser) {
        if (search.isEmpty()) {
            return getAll(loggedInUser);
        }
        List<String> parameters = new ArrayList<>();
        parameters.add(FIRST_NAME);
        parameters.add(LAST_NAME);
        parameters.add(EMAIL);
        parameters.add(PHONE);
        parameters.add(BRAND);
        parameters.add(MODEL);
        return userRepository.search(search.get(), parameters);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user;
            if (username.contains("@")){
                user = userRepository.getByField("email", username);
            } else { user = userRepository.getByField("userName", username); }
            LOGGER.info(FOUND_USER_BY_USERNAME + username);
            user.setLastLoginDateDisplay(user.getLastLoginDate());
            user.setLastLoginDate(new Date());
            userRepository.update(user);
            return user;
        } catch (EntityNotFoundException e){
            LOGGER.error(NO_USER_FOUND_BY_USERNAME + username);
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + username);
        }
    }

    @Override
    public User getById(User loggedInUser, int id) {
        if (loggedInUser.getUserId() != id && !loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_OWNER);
        }
        try {
            return userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", "ID", String.valueOf(id));
        }
    }

    @Override
    public <V> User getByField(String name, V value) {
        return userRepository.getByField(name, value);
    }

    @Override
    public void create(User user) throws DuplicateEntityException {
        throwIfUsernameExists(user);
        throwIfEmailExists(user);
        throwIfPhoneExists(user);
        userRepository.create(user);
    }

    @Override
    public void update(User target, User performer) {
        if (target.getUserId() != performer.getUserId() && !performer.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_OWNER);
        }
        throwIfUsernameExists(target);
        throwIfEmailExists(target);
        throwIfPhoneExists(target);
        userRepository.update(target);
    }

    @Override
    public void updateUserPassword(User loggedInUser, int userId, String encryptedPassword) {
        if (loggedInUser.getUserId() != userId) {
            throw new UnauthorizedOperationException(NOT_OWNER);
        }
        User user = getById(loggedInUser, userId);
        user.setPassword(encryptedPassword);
        userRepository.update(user);
    }

    @Override
    public boolean userExists(String key, String value) {
        return userRepository.userExists(key, value);
    }

    @Override
    public String enable(User loggedInUser, int userId) {
        if (!loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        }
        User userToBeEnabled = getById(loggedInUser, userId);
        userToBeEnabled.setEnabled(true);
        userRepository.update(userToBeEnabled);
        return userToBeEnabled.getUserName();
    }

    @Override
    public String disable(User loggedInUser, int userId) {
        if (!loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        }
        User userToBeEnabled = getById(loggedInUser, userId);
        userToBeEnabled.setEnabled(false);
        userRepository.update(userToBeEnabled);
        return userToBeEnabled.getUserName();
    }

    @Override
    public void passwordReset(User user, String encryptedPassword) {
        //TODO TOken validation
        user.setPassword(encryptedPassword);
        user.setEnabled(true);
        userRepository.update(user);
    }

    @Override
    public void delete(User loggedInUser, int id) {
        if (loggedInUser.getUserId() != id && !loggedInUser.isEmployee()) {
            throw new UnauthorizedOperationException(NOT_OWNER);
        }
        User user = userRepository.getById(id);
        userRepository.delete(user);
    }

    public void throwIfEmailExists(User userToBe) {
        try {
            User existingUser = userRepository.getByField("email", userToBe.getEmail());
            if (existingUser.getUserId() == userToBe.getUserId()) {
                return;
            }
        } catch (EntityNotFoundException e) {
            return;
        }
        throw new DuplicateEntityException("User", "email", userToBe.getEmail());
    }

    public void throwIfUsernameExists(User userToBe) {
        try {
            User existingUser = userRepository.getByField("userName", userToBe.getUserName());
            if (existingUser.getUserId() == userToBe.getUserId()) {
                return;
            }
        } catch (EntityNotFoundException e) {
            return;
        }
        throw new DuplicateEntityException("User", "userName", userToBe.getUserName());
    }

    public void throwIfPhoneExists(User userToBe) {
        try {
            User existingUser = userRepository.getByField("phone", userToBe.getPhone());
            if (existingUser.getUserId() == userToBe.getUserId()) {
                return;
            }
        } catch (EntityNotFoundException e) {
            return;
        }
        throw new DuplicateEntityException("User", "phone", userToBe.getPhone());
    }
}
