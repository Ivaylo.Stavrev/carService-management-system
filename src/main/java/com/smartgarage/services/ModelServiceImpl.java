package com.smartgarage.services;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.repositories.contracts.ModelRepository;
import com.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public void create(CarModel newCarModel) {
        try {
            CarModel model = modelRepository.getByName(newCarModel.getName());
            model.setActive(true);
            modelRepository.update(model);
        } catch (EntityNotFoundException e) {
            modelRepository.create(newCarModel);
        }
    }

    @Override
    public List<CarModel> getAllModels() {
        return modelRepository.getAllModels();
    }

    @Override
    public List<String> getModelNamesByBrandName(String brandName) {
        return modelRepository.getModelNamesByBrandName(brandName);
    }

    @Override
    public CarModel getByModelName(String name) {
        return modelRepository.getByField("name", name);
    }

    @Override
    public List<String> getModelsNotInUse() {
        return modelRepository.getModelsNotInUse();
    }

    public void update(CarModel existingModel) {
        modelRepository.update(existingModel);
    }

    @Override
    public void remove(String modelName) {
        try {
            CarModel modelToRemove = modelRepository.getByName(modelName);
            modelToRemove.setActive(false);
            modelRepository.update(modelToRemove);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Model", "name", modelName);
        }
    }
}
