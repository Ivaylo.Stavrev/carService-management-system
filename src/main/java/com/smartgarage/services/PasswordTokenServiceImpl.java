package com.smartgarage.services;

import com.smartgarage.models.User;
import com.smartgarage.models.tokens.PasswordToken;
import com.smartgarage.repositories.contracts.PasswordTokenRepository;
import com.smartgarage.services.contracts.PasswordTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordTokenServiceImpl implements PasswordTokenService {

    private final PasswordTokenRepository passwordTokenRepository;

    @Autowired
    public PasswordTokenServiceImpl(PasswordTokenRepository passwordResetTokenRepository) {
        this.passwordTokenRepository = passwordResetTokenRepository;
    }

    @Override
    public void delete(int tokenId) {
        PasswordToken passwordToken = passwordTokenRepository.getById(tokenId);
        passwordTokenRepository.delete(passwordToken);
    }

    @Override
    public PasswordToken getPasswordToken(String passwordToken) {
        return passwordTokenRepository.getByField("passwordToken", passwordToken);
    }

    @Override
    public void createPasswordTokenForUser(User user, String passwordToken) {
        PasswordToken myToken = new PasswordToken();
        myToken.setPasswordToken(passwordToken);
        myToken.setUser(user);
        passwordTokenRepository.create(myToken);
    }
}
