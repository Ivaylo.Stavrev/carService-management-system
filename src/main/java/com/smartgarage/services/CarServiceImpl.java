package com.smartgarage.services;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.enums.CarSortOptions;
import com.smartgarage.repositories.contracts.CarRepository;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.services.contracts.CarService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.smartgarage.constant.UserImplConstant.*;

@Service
public class CarServiceImpl implements CarService {

    public static final String NOT_OWNER_OR_EMPLOYEE = "Only employees and owners are allowed to modify a car!";
    public static final String NOT_EMPLOYEE = "Only employees are allowed to search through all cars.";

    public static final String FIRST_NAME = "u.firstName";
    public static final String LAST_NAME = "u.lastName";
    public static final String EMAIL = "u.email";
    public static final String PHONE = "u.phone";
    public static final String PLATE = "c.licensePlate";
    public static final String VIN = "c.vin";

    private final CarRepository carRepository;
    private final VisitService visitService;
    private final BrandService brandService;
    private final ModelService modelService;

    @Autowired
    public CarServiceImpl(CarRepository carRepository,
                          VisitService visitService, BrandService brandService,
                          ModelService modelService) {
        this.carRepository = carRepository;
        this.visitService = visitService;
        this.brandService = brandService;
        this.modelService = modelService;
    }

    @Override
    public List<Car> getAll(User loggedInUser) {
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        return carRepository.getAll();
    }

    private List<Car> search(Optional<String> search, List<String> parameters) {
        if (search.isPresent()) {
            return carRepository.search(search.get(), parameters);
        }
        return carRepository.getAll();
    }

    // Employees must be able to filter the vehicles by owner. (first name, last name, email, phone)
    @Override
    public List<Car> searchFromMVC(Optional<String> search, User loggedInUser) {
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        List<String> parameters = new ArrayList<>();
        parameters.add(FIRST_NAME);
        parameters.add(LAST_NAME);
        parameters.add(EMAIL);
        parameters.add(PHONE);
        return this.search(search, parameters);
    }

    // Employee must be able to search vehicles by license plate, VIN or owner’s phone number
    @Override
    public List<Car> searchFromREST(Optional<String> search, User loggedInUser) {
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        List<String> parameters = new ArrayList<>();
        parameters.add(PLATE);
        parameters.add(VIN);
        parameters.add(PHONE);
        return this.search(search, parameters);
    }

    @Override
    public Car getById(User loggedInUser, int carId) {
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        return carRepository.getById(carId);
    }

    @Override
    public <V> Car getByField(User loggedInUser, String name, V value) {
        if (!loggedInUser.isEmployee()) throw new UnauthorizedOperationException(NOT_EMPLOYEE);
        return carRepository.getByField(name, value);
    }

    @Override
    public List<Car> filter(Optional<String> brandName,
                            Optional<String> modelName,
                            Optional<Integer> year,
                            Optional<CarSortOptions> sortOptions) {
        return carRepository.filter(brandName, modelName, year, sortOptions);
    }

    @Override
    public void create(User performer, Car newCar) {
        validatePerformer(performer, newCar);

        CarModel carModel = newCar.getCarModel();
        Brand brand = newCar.getBrand();
        try {
            brand = brandService.getByBrandName(newCar.getBrand().getName());
        } catch (EntityNotFoundException e) {
            brand.setModels(new ArrayList<>());
            brand.getModels().add(carModel);
            brandService.create(brand);
        }
        try {
            carModel = modelService.getByModelName(newCar.getCarModel().getName());
        } catch (EntityNotFoundException e) {
            carModel = new CarModel(newCar.getCarModel().getName());
            modelService.create(carModel);
            List<CarModel> carModels = new ArrayList<>();
            carModels.add(carModel);
            brand.setModels(carModels);
            brandService.update(brand);
        }
        newCar.setBrand(brand);
        newCar.setCarModel(carModel);
        carRepository.create(newCar);
    }
    @Override
    public void update(User performer, Car target) {
        validatePerformer(performer, target);
        carRepository.update(target);
    }

    @Override
    public void delete(User performer, int carId) throws EntityNotFoundException, UnauthorizedOperationException{
        Car car;
        try {
            car = carRepository.getById(carId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Car", "ID", String.valueOf(carId));
        }
        validatePerformer(performer, car);
        List<Visit> visits = visitService.getVisitsByCarId(car.getCarId(), performer);
        if (!visits.isEmpty()) throw new UnauthorizedOperationException(CAR_WITH_VISITS_DELETE_ERROR);
        carRepository.delete(car);
    }
    private void validatePerformer(User loggedInUser, Car newCar) {
        if (loggedInUser.getUserId() != newCar.getUser().getUserId() && !loggedInUser.isEmployee())
            throw new UnauthorizedOperationException(NOT_OWNER_OR_EMPLOYEE);
    }
}
