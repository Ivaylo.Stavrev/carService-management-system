package com.smartgarage.services;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.repositories.contracts.BrandRepository;
import com.smartgarage.services.contracts.BrandService;
import com.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;
    private final ModelService modelService;

    @Autowired
    public BrandServiceImpl(BrandRepository brandRepository, ModelService modelService) {
        this.brandRepository = brandRepository;
        this.modelService = modelService;
    }

    @Override
    public void create(Brand newBrand) {
        CarModel model = newBrand.getModels().get(0); //model without id
        modelService.create(model);
        model = modelService.getByModelName(model.getName()); //model from db with id
        try {
            Brand brand = brandRepository.getByName(newBrand.getName());
            brand.setActive(true);
            List<CarModel> modelsInCurrentBrand = brand.getModels();
            if (!modelsInCurrentBrand.contains(model)) {
                modelsInCurrentBrand.add(model);
            }
            this.update(brand);
        } catch (EntityNotFoundException e) {
            brandRepository.create(newBrand);
        }
    }

    @Override
    public void update(Brand existingBrand) {
        brandRepository.update(existingBrand);
    }

    @Override
    public List<Brand> getAllBrands() {
        return brandRepository.getAllBrands();
    }

    @Override
    public Brand getByBrandName(String name) {
        return brandRepository.getByName(name);
    }

    @Override
    public void remove(String brandName) {
        try {
            Brand brandToRemove = brandRepository.getByName(brandName);
            brandRepository.removeModelsByBrandName(brandName);
            brandToRemove.setActive(false);
            this.update(brandToRemove);
//            brandToRemove.getModels().forEach(model -> modelService.remove(model.getName()));
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Brand", "name", brandName);
        }
    }

}
