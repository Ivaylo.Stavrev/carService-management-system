package com.smartgarage.services;

import com.posadskiy.currencyconverter.CurrencyConverter;
import com.posadskiy.currencyconverter.enums.Currency;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.repositories.contracts.CarRepository;
import com.smartgarage.repositories.contracts.VisitRepository;
import com.smartgarage.services.contracts.PdfGenerateService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

import static java.lang.String.format;

@Service
public class VisitServiceImpl implements VisitService {
    public static final String NOT_OWNER_OR_EMPLOYEE = "Only employees and owners are allowed to modify a visit!";

    private final VisitRepository visitRepository;
    private final CarRepository carRepository;
    private final CurrencyConverter currencyConverter;
    private final PdfGenerateService pdfGenerateService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            CarRepository carRepository, CurrencyConverter currencyConverter, PdfGenerateService pdfGenerateService) {
        this.visitRepository = visitRepository;
        this.carRepository = carRepository;
        this.currencyConverter = currencyConverter;
        this.pdfGenerateService = pdfGenerateService;
    }

    @Override
    public List<Visit> getVisitsByCarId(int carId, User loggedInUser) {
        //todo validation
        return visitRepository.getVisitsByCarId(carId);
    }

    @Override
    public List<Visit> getAll() {
        return visitRepository.getAll();
    }

    @Override
    public List<Visit> getAll(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        return visitRepository.search(search.get());
    }

    @Override
    public Visit getById(int visitId, User loggedInUser) {
        if (!loggedInUser.isEmployee() && loggedInUser.getVisits().stream()
                .map(Visit::getVisitId).noneMatch(id -> id == visitId )) {
            throw new UnauthorizedOperationException(NOT_OWNER_OR_EMPLOYEE);
        }
        return visitRepository.getById(visitId);
    }

    @Override
    public void create(Visit newVisit) {
        visitRepository.create(newVisit);
    }

    @Override
    public void create(Visit newVisit, User performer) {
        this.create(newVisit);
    }

    @Override
    public void update(Visit visit) {
        visitRepository.update(visit);
    }

    @Override
    public void update(Visit visit, User performer) {
        this.update(visit);
    }

    @Override
    public void delete(Visit visit) {
        visitRepository.delete(visit);
    }

    @Override
    public void delete(Visit visit, User performer) {

        this.delete(visit);
    }

    @Override
    public void createVisitForCarWithCarId(int carId, User loggedInUser) {
        Car currentCar = carRepository.getById(carId);
        User owner = currentCar.getUser();
        Visit newVisit = new Visit(LocalDateTime.now(), currentCar, owner);
        this.create(newVisit);
    }

    @Override
    public void checkOut(int visitId, User loggedInUser) {
        Visit visit = visitRepository.getById(visitId);
        visit.setCheckOut(LocalDateTime.now());
        this.update(visit);
    }

    @Override
    public List<Visit> getVisitsByCarServiceId(int serviceId) {
        return visitRepository.getVisitsByCarServiceId(serviceId);
    }

    @Override
    public double getExchangeRate(User loggedInUser, String currencyName, Visit visit) throws EntityNotFoundException{
        Currency newCurrency = Arrays.stream(Currency.values())
                .filter(c -> c.name().equalsIgnoreCase(currencyName))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("currency", "name", currencyName));
        return currencyConverter.rate(Currency.BGN, newCurrency);
    }

    @Override
    public String createPdf(Visit visit, User loggedInUser, Optional<Double> exchangeRate,
                            Optional<String> currency) throws UnauthorizedOperationException, IOException {
        validatePerformer(loggedInUser, visit);
        String licencePlate = visit.getCar().getLicensePlate();
        String owner = visit.getUser().getFirstName() + " " + visit.getUser().getLastName();
        String fileName = format("%s_%s_%s.pdf", licencePlate, owner, visit.showCheckInDate());
        return pdfGenerateService.generatePdfFile(visit, exchangeRate, currency, fileName);
    }

    private void validatePerformer(User loggedInUser, Visit visit) {
        if (loggedInUser.getUserId() != visit.getUser().getUserId() && !loggedInUser.isEmployee())
            throw new UnauthorizedOperationException(NOT_OWNER_OR_EMPLOYEE);
    }
}
