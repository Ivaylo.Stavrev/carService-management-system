package com.smartgarage.controllers.rest;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.dto.CarServiceDTO;
import com.smartgarage.models.enums.ServiceSortOptions;
import com.smartgarage.services.contracts.CarServiceService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.CarServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/services")
public class ServicesController {

    private final CarServiceService service;
    private final AuthenticationHelper authenticationHelper;
    private final CarServiceMapper mapper;


    @Autowired
    public ServicesController(CarServiceService service,
                              AuthenticationHelper authenticationHelper,
                              CarServiceMapper mapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @GetMapping
    public List<CarService> getAllServices(@RequestParam(required = false) Optional<String> search) {
        User loggedInUser = authenticationHelper.tryGetUser();
        if (search.isEmpty()) {
            return service.getAll();
        }
        return service.getAll().stream()
                .filter(carService -> carService.getName().toLowerCase().contains(search.get().toLowerCase()))
                .collect(Collectors.toList());
    }

    @GetMapping("/filter")
    public List<CarService> filter(@RequestParam(required = false) Optional<String> name,
                             @RequestParam(required = false) Optional<Double> minPrice,
                             @RequestParam(required = false) Optional<Double> maxPrice,
                             @RequestParam(required = false) Optional<String> sort) {
        User loggedInUser = authenticationHelper.tryGetUser();
        return service.filter(name, minPrice, maxPrice, sort.map(ServiceSortOptions::valueOf));
    }

    @PostMapping
    public CarService create(@Valid @RequestBody CarServiceDTO carServiceDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            CarService carService = mapper.createServiceFromDTO(carServiceDTO);
            service.create(loggedInUser, carService);
            return carService;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CarService update(@PathVariable int id, @Valid @RequestBody CarServiceDTO carServiceDTO) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            CarService carService = mapper.createServiceFromDTOUpdate(loggedInUser, carServiceDTO, id);
            service.update(loggedInUser, carService);
            return carService;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            service.delete(loggedInUser, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
