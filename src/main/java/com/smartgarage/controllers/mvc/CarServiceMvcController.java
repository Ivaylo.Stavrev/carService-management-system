package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.DuplicateEntityException;
import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.*;
import com.smartgarage.models.dto.CarServiceDTO;
import com.smartgarage.services.contracts.CarServiceService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VisitService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.CarServiceMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

import static java.lang.String.format;

@Controller
@RequestMapping("/services")
public class CarServiceMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final com.smartgarage.services.contracts.CarService carService;
    private final UserService userService;
    private final VisitService visitService;
    private final CarServiceService carServiceService;
    private final CarServiceMapper carServiceMapper;

    public CarServiceMvcController(AuthenticationHelper authenticationHelper,
                                   com.smartgarage.services.contracts.CarService carService, UserService userService,
                                   VisitService visitService, CarServiceService carServiceService, CarServiceMapper carServiceMapper) {
        this.authenticationHelper = authenticationHelper;
        this.carService = carService;
        this.userService = userService;
        this.visitService = visitService;
        this.carServiceService = carServiceService;
        this.carServiceMapper = carServiceMapper;
    }

    @ModelAttribute("cars")
    public List<Car> getAllCars() {
        User loggedInUser = authenticationHelper.tryGetUser();
        return carService.getAll(loggedInUser);
    }

    @ModelAttribute("users")
    public List<User> getAllUsers() {
        User loggedInUser = authenticationHelper.tryGetUser();
        return userService.getAll(loggedInUser);
    }

    @ModelAttribute("carServices")
    public List<CarService> getAllCarServices() {
        return carServiceService.getAll();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().getName() != null;
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Employee"));
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    @GetMapping
    public String getAll() {
        return "service/services";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("newService", new CarServiceDTO());
        return "service/service-create";
    }

    @PostMapping("/create")
    public String create(RedirectAttributes redirectAttributes,
                         @Valid @ModelAttribute("newService") CarServiceDTO serviceDTO,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "service/service-create";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            CarService newCarService = carServiceMapper.createServiceFromDTO(serviceDTO);
            carServiceService.create(loggedInUser, newCarService);
            redirectAttributes.addAttribute("success", "Service successfully created.");
            return "redirect:/services";
        } catch (UnauthorizedOperationException | DuplicateEntityException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/services";
        } catch (NumberFormatException e) {
            redirectAttributes.addAttribute("error", format("Invalid number:%s", serviceDTO.getPrice()));
            return "redirect:service/service-create";
        }
    }

    @GetMapping("/{serviceId}")
    public String getById(Model model, @PathVariable int serviceId) {
        CarService carService = carServiceService.getById(getLoggedInUser(), serviceId);
        List<Visit> serviceVisits = carServiceService.getVisitsByCarServiceId(serviceId);
        CarServiceDTO carServiceDTO = carServiceMapper.createCarServiceDTOFromCarService(carService);
        model.addAttribute("carService", carService);
        model.addAttribute("service", carServiceDTO);
        model.addAttribute("visits", serviceVisits);
        return "service/service";
    }

    @PostMapping("/{serviceId}/update")
    public String getById(RedirectAttributes redirectAttributes,
                          @PathVariable int serviceId,
                          @Valid @ModelAttribute("service") CarServiceDTO carServiceDTO,
                            BindingResult errors) {
        if (errors.hasErrors()) {
            redirectAttributes.addAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/services/{serviceId}";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            CarService carService = carServiceMapper.createServiceFromDTOUpdate(loggedInUser, carServiceDTO, serviceId);
            carServiceService.update(loggedInUser, carService);
            redirectAttributes.addAttribute("success", "Successful service update");
            return "redirect:/services/{serviceId}";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/services/{serviceId}";
        }
    }

    @PostMapping("/{serviceId}/delete")
    public String delete(RedirectAttributes redirectAttributes,
                         @PathVariable int serviceId){
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            carServiceService.delete(loggedInUser, serviceId);
            redirectAttributes.addAttribute("success", "Successful service delete");
            return "redirect:/services";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/services/{serviceId}";
        }
    }

    @PostMapping("/{visitId}/remove/{serviceId}")
    public String removeServiceFromVisit(RedirectAttributes redirectAttributes,
                                        @PathVariable int visitId,
                                         @PathVariable int serviceId) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            CarService carService = carServiceService.getById(loggedInUser, serviceId);
            visit.getServices().remove(carService);
            visitService.update(visit);
            redirectAttributes.addAttribute("success", "Successful service removal from visit");
            return "redirect:/services/{serviceId}";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/services/{serviceId}";
        }
    }
}