package com.smartgarage.controllers.mvc;

import com.smartgarage.exceptions.models.EntityNotFoundException;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.Role;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.attributes.Brand;
import com.smartgarage.models.attributes.CarModel;
import com.smartgarage.models.dto.CarDTO;
import com.smartgarage.services.contracts.*;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.CarMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/cars")
public class CarMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CarService carService;
    private final UserService userService;
    private final ModelService modelService;
    private final BrandService brandService;
    private final VisitService visitService;
    private final CarMapper carMapper;

    public CarMvcController(AuthenticationHelper authenticationHelper,
                            CarService carService, UserService userService,
                            ModelService modelService, BrandService brandService,
                            VisitService visitService, CarMapper carMapper) {
        this.authenticationHelper = authenticationHelper;
        this.carService = carService;
        this.userService = userService;
        this.modelService = modelService;
        this.brandService = brandService;
        this.visitService = visitService;
        this.carMapper = carMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().getName() != null;
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Employee"));
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    @ModelAttribute("models")
    public List<CarModel> getAllModels() {
        return modelService.getAllModels();
    }

    @ModelAttribute("brands")
    public List<Brand> getAllBrands() {
        return brandService.getAllBrands();
    }

    @ModelAttribute("customers")
    public List<User> getAllCustomers() {
        User loggedInUser = authenticationHelper.tryGetUser();
        return userService.getAllCustomers(loggedInUser);
    }

    @GetMapping
    public String showAllCars(RedirectAttributes redirectAttributes,
                              @RequestParam(required = false) Optional<String> search, Model model) {

        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            model.addAttribute("cars", carService.searchFromMVC(search, loggedInUser));
            return "car/cars";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/cars";
        }
    }

    @GetMapping("/create")
    public String create(Model model, @RequestParam(required = false, name = "userId") Optional<Integer> userId) {
        CarDTO car = new CarDTO();
        userId.ifPresent(id -> car.setUserName(userService.getById(getLoggedInUser(), id).getUserName()));
        model.addAttribute("newCar", car);
        return "car/car-create";
    }

    @PostMapping("/create")
    public String create(Model model,
                         RedirectAttributes redirectAttributes,
                         @Valid @ModelAttribute("newCar") CarDTO carDTO,
                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Unsuccessful create.");
            return "car/car-create";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Car newCar = carMapper.createCarFromDTO(carDTO);
            carService.create(loggedInUser, newCar);
            redirectAttributes.addAttribute("success", "Car successfully create.");
            return "redirect:/cars";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/cars";
        }
    }

    @RequestMapping(value = "/models")
    @ResponseBody
    public List<String> getModels(@RequestParam ("brand") String brandName) {
        List<String> modelNames = modelService.getModelNamesByBrandName(brandName);
        modelNames.sort(String::compareTo);
        return modelNames;
    }

    @GetMapping("/{carId}")
    public String getById(Model model, RedirectAttributes redirectAttributes, @PathVariable int carId) {

        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Car existingCar = carService.getById(loggedInUser, carId);
            CarDTO carDTO = carMapper.createCarDTOfromCar(existingCar);
            List<Visit> visits = visitService.getVisitsByCarId(carId, loggedInUser);
            model.addAttribute("visits", visits);
            model.addAttribute("currentCar", existingCar);
            model.addAttribute("car", carDTO);
            return "car/car";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/users/{userId}";
        }
    }


    @PostMapping("/{carId}/update")
    public String update(RedirectAttributes redirectAttributes,
                         @PathVariable int carId,
                         @Valid @ModelAttribute("car") CarDTO carDTO,
                         BindingResult errors) {
        if (errors.hasErrors()) {
            List<String> errorMessages = errors.getAllErrors()
                    .stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toList());
            redirectAttributes.addAttribute("error", String.join("\n", errorMessages));
            return "redirect:/cars/{carId}";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Car car = carMapper.createCarFromDTOUpdate(loggedInUser, carDTO, carId);
            carService.update(loggedInUser, car);
            redirectAttributes.addAttribute("success", "Successful car update");
            return "redirect:/cars/{carId}";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/cars/{carId}";
        }
    }

    @PostMapping("/{carId}/delete")
    public String delete(RedirectAttributes redirectAttributes,
                         @PathVariable int carId) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            carService.delete(loggedInUser, carId);
            redirectAttributes.addAttribute("success", "Successful car delete");
            return "redirect:/cars";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/cars/{carId}";
        }
    }

    // Create Visit INSTANTLY for particular car
    @PostMapping("/{carId}")
    public String createVisitForCarWithCarId(RedirectAttributes redirectAttributes,
                                             @PathVariable int carId) {
        User loggedInUser = authenticationHelper.tryGetUser();
        visitService.createVisitForCarWithCarId(carId, loggedInUser);
        redirectAttributes.addAttribute("success", "Visit successfully created.");
        return "redirect:/cars/{carId}";
    }
}
