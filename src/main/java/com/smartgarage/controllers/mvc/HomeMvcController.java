package com.smartgarage.controllers.mvc;

import com.posadskiy.currencyconverter.enums.Currency;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.services.contracts.VisitService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final VisitService visitService;

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return !SecurityContextHolder.getContext().getAuthentication().getName()
                .equalsIgnoreCase("anonymousUser");
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Employee"));
    }

    @ModelAttribute("isCustomer")
    public boolean isCustomer() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Customer"));
    }

    @ModelAttribute("visits")
    public List<Visit> getAllVisits() {
        return visitService.getAll();
    }

    @ModelAttribute("currencies")
    public List<String> getCurrencies() {
        return Stream.of(Currency.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    public HomeMvcController(AuthenticationHelper authenticationHelper, VisitService visitService) {
        this.authenticationHelper = authenticationHelper;
        this.visitService = visitService;
    }

    @GetMapping("/")
    public String showHomePage(Model model){
        return "index";
    }
}
