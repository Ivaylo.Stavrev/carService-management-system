package com.smartgarage.controllers.mvc;

import com.posadskiy.currencyconverter.enums.Currency;
import com.smartgarage.events.OnEmailPdfCompleteEvent;
import com.smartgarage.exceptions.models.UnauthorizedOperationException;
import com.smartgarage.models.Car;
import com.smartgarage.models.CarService;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dto.CarServiceDTO;
import com.smartgarage.models.dto.VisitDTO;
import com.smartgarage.services.contracts.CarServiceService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VisitService;
import com.smartgarage.utils.helper.AuthenticationHelper;
import com.smartgarage.utils.mapper.VisitMapper;
import org.hibernate.action.internal.EntityActionVetoException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final VisitService visitService;
    private final com.smartgarage.services.contracts.CarService carService;
    private final UserService userService;
    private final CarServiceService carServiceService;
    private final VisitMapper visitMapper;
    private final ApplicationEventPublisher eventPublisher;

    public VisitMvcController(AuthenticationHelper authenticationHelper,
                              VisitService visitService,
                              com.smartgarage.services.contracts.CarService carService,
                              UserService userService,
                              CarServiceService carServiceService,
                              VisitMapper visitMapper, ApplicationEventPublisher eventPublisher) {
        this.authenticationHelper = authenticationHelper;
        this.visitService = visitService;
        this.carService = carService;
        this.userService = userService;
        this.carServiceService = carServiceService;
        this.visitMapper = visitMapper;
        this.eventPublisher = eventPublisher;
    }

    @ModelAttribute("cars")
    public List<Car> getAllCars() {
        User loggedInUser = authenticationHelper.tryGetUser();
        return carService.getAll(loggedInUser);
    }

    @ModelAttribute("users")
    public List<User> getAllUsers() {
        User loggedInUser = authenticationHelper.tryGetUser();
        return userService.getAll(loggedInUser);
    }

    @ModelAttribute("carServices")
    public List<CarService> getAllServices() {
        return carServiceService.getAll();
    }

    @ModelAttribute("currencies")
    public List<String> getCurrencies() {
        return Stream.of(Currency.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication().getName() != null;
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equals("ROLE_Employee"));
    }

    @ModelAttribute("loggedInUser")
    public User getLoggedInUser() {
        if (populateIsAuthenticated()) return authenticationHelper.tryGetUser();
        return null;
    }

    @GetMapping
    public String showAllVisits(
            @RequestParam(required = false) Optional<String> search, Model model) {

        try {
            model.addAttribute("visits", visitService.getAll(search));
            return "visit/visits";
        } catch (UnauthorizedOperationException e) {
            return "/authentication/access-denied";
        }
    }

    @GetMapping("/{visitId}")
    public String showVisitById(@PathVariable int visitId, Model model) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            model.addAttribute("visit", visit);
            model.addAttribute("service", new CarServiceDTO());
            return "visit/visit";
        } catch (UnauthorizedOperationException e) {
            return "/authentication/access-denied";
        }
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("newVisit", new VisitDTO());
        return "visit/visit-create";
    }

    @GetMapping(value = "/{visitId}/pdf", produces = APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> getPDF (Model model,
                     @PathVariable int visitId,
                     @RequestParam(value = "currency", required = false) Optional<String> currency,
                    @RequestParam(value = "exchangeRate", required = false) Optional<Double> exchangeRate) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Visit visitDB = visitService.getById(visitId, loggedInUser);
            String path = visitService.createPdf(visitDB, loggedInUser, exchangeRate, currency);
            byte[] pdfContent = Files.readAllBytes(Paths.get(path));
            Files.delete(Path.of(path));
            model.addAttribute("visit", visitDB);
            exchangeRate.ifPresent(ex -> model.addAttribute("exchangeRate", ex));
            currency.ifPresent(c -> model.addAttribute("currency", c));
            return new ResponseEntity<>(pdfContent, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(new byte[0], HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/{visitId}/email/pdf")
    public String getPDF (RedirectAttributes redirectAttributes,
                    @PathVariable int visitId,
                      @RequestParam(value = "visits", required = false) Optional<String> visits,
                      @RequestParam(value = "currency", required = false) Optional<String> currency,
                      @RequestParam(value = "exchangeRate", required = false) Optional<Double> exchangeRate){
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            String path = visitService.createPdf(visit, loggedInUser, exchangeRate, currency);
            eventPublisher.publishEvent(new OnEmailPdfCompleteEvent(visit, path));
            Files.delete(Path.of(path));
            if (visits.isPresent()){
                redirectAttributes.addAttribute("success",
                                "An email was sent to customer with email: " + visit.getUser().getEmail());
                return "redirect:/";
            }
            redirectAttributes.addAttribute
                    ("success",
                            "An email was sent to customer with email: " + visit.getUser().getEmail());
            return "redirect:/visits/{visitId}";
        } catch (IOException e) {
            redirectAttributes.addAttribute
                    ("error", "Unsuccessful email sent!");
            return "redirect:/visits/{visitId}";
        }
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("newVisit") VisitDTO visitDTO,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "visit/visit-create";
        }
        User loggedInUser = authenticationHelper.tryGetUser();
        Visit newVisit = visitMapper.createVisitFromDTO(loggedInUser, visitDTO);
        visitService.create(newVisit, loggedInUser);
        return "redirect:/visits";
    }

    @PostMapping("/{visitId}")
    public String addServiceToCurrentVisit(RedirectAttributes redirectAttributes,
                                           @PathVariable int visitId,
                                           @ModelAttribute("service") CarServiceDTO carServiceDTO) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            CarService carService = carServiceService.getByName(carServiceDTO.getName());
            visit.getServices().add(carService);
            visitService.update(visit, loggedInUser);
            redirectAttributes.addAttribute("success", "Service successfully added to visit.");
            return "redirect:/visits/{visitId}";
        } catch (UnauthorizedOperationException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/visits/{visitId}";
        }
    }

    @PostMapping("/{visitId}/remove-service/{serviceId}")
    public String removeServiceFromVisit(@PathVariable int visitId,
                                         @PathVariable int serviceId) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            CarService carService = carServiceService.getById(loggedInUser, serviceId);
            visit.getServices().remove(carService);
            visitService.update(visit, loggedInUser);
            return "redirect:/visits/{visitId}";
        } catch (UnauthorizedOperationException e) {
            return "/authentication/access-denied";
        }
    }

    @PostMapping("/{visitId}/currency")
    public String changeCurrency(Model model,
                                 RedirectAttributes redirectAttributes,
                                 @PathVariable int visitId,
                                @RequestParam("currency") String currency) {
        try {
            User loggedInUser = authenticationHelper.tryGetUser();
            Visit visit = visitService.getById(visitId, loggedInUser);
            double exchangeRate = visitService.getExchangeRate(loggedInUser, currency, visit);
            model.addAttribute("currency", currency);
            model.addAttribute("exchangeRate", exchangeRate);
            model.addAttribute("visit", visit);
            model.addAttribute("service", new CarServiceDTO());
            return "visit/visit";
        } catch (UnauthorizedOperationException | EntityActionVetoException e) {
            redirectAttributes.addAttribute("error", e.getMessage());
            return "redirect:/visits/{visitId}";
        }
    }

    @PostMapping("/{visitId}/checkout")
    public String checkOut(@PathVariable int visitId) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            visitService.checkOut(visitId, loggedInUser);
            return "redirect:/visits/{visitId}";
        } catch (UnauthorizedOperationException e) {
            return "/authentication/access-denied";
        }
    }

    @PostMapping("/{visitId}/delete")
    public String deleteVisit(@PathVariable int visitId) {
        User loggedInUser;
        try {
            loggedInUser = authenticationHelper.tryGetUser();
            Visit visitToDelete = visitService.getById(visitId, loggedInUser);
            visitService.delete(visitToDelete, loggedInUser);
            return "redirect:/visits";
        } catch (UnauthorizedOperationException e) {
            return "/authentication/access-denied";
        }
    }
}