package com.smartgarage.events;

import com.smartgarage.models.User;
import org.springframework.context.ApplicationEvent;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final User user;

    private final String registerType;

    public OnRegistrationCompleteEvent(User user, String registerType) {
        super(user);
        this.user = user;
        this.registerType = registerType;
    }

    public User getUser() {
        return user;
    }

    public String getRegisterType() {
        return registerType;
    }
}
