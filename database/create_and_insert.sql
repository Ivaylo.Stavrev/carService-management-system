-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for smartgarage
CREATE DATABASE IF NOT EXISTS `smartgarage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartgarage`;

-- Dumping structure for table smartgarage.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`brand_id`),
  UNIQUE KEY `brands_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.brands: ~13 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`brand_id`, `name`, `is_active`) VALUES
	(1, 'Toyota', 1),
	(2, 'Opel', 1),
	(5, 'Ford', 1),
	(6, 'BMW', 1),
	(7, 'Audi', 1),
	(8, 'Mercedes Benz', 1),
	(9, 'Honda', 1),
	(10, 'Renault', 1),
	(11, 'Peugeot', 1),
	(14, 'Jaguar', 1),
	(15, 'Dodge', 1),
	(30, 'Nisan', 1),
	(31, 'SEAT', 1);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table smartgarage.brands_models
CREATE TABLE IF NOT EXISTS `brands_models` (
  `brand_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  KEY `brands_models_brands_brand_id_fk` (`brand_id`),
  KEY `brands_models_models_model_id_fk` (`model_id`),
  CONSTRAINT `brands_models_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `brands_models_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.brands_models: ~48 rows (approximately)
/*!40000 ALTER TABLE `brands_models` DISABLE KEYS */;
INSERT INTO `brands_models` (`brand_id`, `model_id`) VALUES
	(14, 13),
	(8, 15),
	(8, 6),
	(8, 7),
	(10, 9),
	(11, 10),
	(11, 11),
	(5, 24),
	(5, 25),
	(5, 26),
	(5, 27),
	(7, 30),
	(7, 31),
	(7, 32),
	(7, 33),
	(7, 34),
	(2, 2),
	(2, 19),
	(2, 16),
	(2, 18),
	(2, 17),
	(9, 35),
	(9, 36),
	(9, 37),
	(9, 38),
	(9, 39),
	(6, 4),
	(6, 5),
	(6, 40),
	(6, 41),
	(6, 43),
	(15, 14),
	(15, 44),
	(15, 57),
	(30, 58),
	(1, 1),
	(1, 3),
	(1, 20),
	(1, 21),
	(1, 22),
	(1, 23),
	(1, 29),
	(1, 59),
	(31, 60),
	(31, 62),
	(31, 63),
	(31, 64);
/*!40000 ALTER TABLE `brands_models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `license_plate` varchar(32) NOT NULL,
  `vin` varchar(32) NOT NULL,
  `creation_year` year(4) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`car_id`),
  KEY `cars_users_user_id_fk` (`user_id`),
  CONSTRAINT `cars_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars: ~10 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`car_id`, `license_plate`, `vin`, `creation_year`, `user_id`) VALUES
	(1, 'CB3449HB', 'JTDAT1230Y5017131', '2005', 1),
	(2, 'A8695MT', 'KMHDU4AD4AU955646', '2010', 2),
	(4, 'CA1212XA', 'SAJWA1C78D8V38055', '2013', 3),
	(5, 'CA4545MB', '1B7GG23Y1NS526835', '1992', 4),
	(6, 'CB0833HK', 'WDBRF52H76F783280', '2006', 5),
	(7, 'PB1234AX', 'RANDOM11111NUMBER', '2016', 2),
	(16, 'PB1771PB', '3B3ES47C6WT211725', '2016', 6),
	(18, 'CA0990MA', '5TFHW5F13AX136128', '2010', 4),
	(19, 'A6204AT', '1B3BG56P3FX563681', '1993', 6),
	(20, 'GE6457KT', 'JN1CA21DXXT805880', '2005', 8);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars_brands
CREATE TABLE IF NOT EXISTS `cars_brands` (
  `car_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  KEY `cars_brands_cars_car_id_fk` (`car_id`),
  KEY `cars_brands_brands_brand_id_fk` (`brand_id`),
  CONSTRAINT `cars_brands_brands_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `cars_brands_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars_brands: ~10 rows (approximately)
/*!40000 ALTER TABLE `cars_brands` DISABLE KEYS */;
INSERT INTO `cars_brands` (`car_id`, `brand_id`) VALUES
	(1, 1),
	(2, 2),
	(4, 14),
	(5, 15),
	(6, 8),
	(7, 8),
	(16, 15),
	(18, 1),
	(19, 31),
	(20, 7);
/*!40000 ALTER TABLE `cars_brands` ENABLE KEYS */;

-- Dumping structure for table smartgarage.cars_models
CREATE TABLE IF NOT EXISTS `cars_models` (
  `car_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  KEY `cars_models_models_model_id_fk` (`model_id`),
  KEY `cars_models_cars_car_id_fk` (`car_id`),
  CONSTRAINT `cars_models_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
  CONSTRAINT `cars_models_models_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.cars_models: ~10 rows (approximately)
/*!40000 ALTER TABLE `cars_models` DISABLE KEYS */;
INSERT INTO `cars_models` (`car_id`, `model_id`) VALUES
	(1, 1),
	(2, 2),
	(4, 13),
	(5, 14),
	(6, 15),
	(7, 6),
	(16, 44),
	(18, 59),
	(19, 60),
	(20, 30);
/*!40000 ALTER TABLE `cars_models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.models
CREATE TABLE IF NOT EXISTS `models` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`model_id`),
  UNIQUE KEY `models_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.models: ~49 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` (`model_id`, `name`, `is_active`) VALUES
	(1, 'Yaris', 1),
	(2, 'Zafira', 1),
	(3, 'Corola', 1),
	(4, 'X3', 1),
	(5, 'X5', 1),
	(6, 'GL500', 1),
	(7, 'CLK 270', 1),
	(8, 'Cougar', 1),
	(9, 'Megane', 1),
	(10, '206', 1),
	(11, '307', 1),
	(13, 'XJ', 1),
	(14, 'Dakota', 1),
	(15, 'C Class', 1),
	(16, 'Kadett', 1),
	(17, 'Tigra', 1),
	(18, 'Vectra', 1),
	(19, 'Insignia', 1),
	(20, 'Aygo', 1),
	(21, 'Hilux', 1),
	(22, 'Avensis', 1),
	(23, 'Prius', 1),
	(24, 'Focus', 1),
	(25, 'Mondeo', 1),
	(26, 'Fiesta', 1),
	(27, 'Mustang', 1),
	(29, 'Sequoia', 1),
	(30, 'A4', 1),
	(31, 'A6', 1),
	(32, 'A8', 1),
	(33, 'Q5', 1),
	(34, 'Q7', 1),
	(35, 'Accord', 1),
	(36, 'Civic', 1),
	(37, 'Jazz', 1),
	(38, 'Legend', 1),
	(39, 'Odyssey', 1),
	(40, 'X1', 1),
	(41, 'X2', 1),
	(43, 'M5', 1),
	(44, 'Ram', 1),
	(57, 'Neon', 1),
	(58, 'Maxima', 1),
	(59, 'Tundra', 1),
	(60, 'Toledo', 1),
	(62, 'Ibiza', 1),
	(63, 'Leon', 1),
	(64, 'Cordoba', 1);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table smartgarage.password_tokens
CREATE TABLE IF NOT EXISTS `password_tokens` (
  `password_token_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  PRIMARY KEY (`password_token_id`),
  KEY `password_tokens_users_user_id_fk` (`user_id`),
  CONSTRAINT `password_tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.password_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_tokens` ENABLE KEYS */;

-- Dumping structure for table smartgarage.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(16) NOT NULL,
  PRIMARY KEY (`role_id`),
  KEY `employees_users_user_id_fk` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role`) VALUES
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table smartgarage.services
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `price` double NOT NULL,
  `status` enum('ACTIVE','INACTIVE'),
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.services: ~12 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`service_id`, `name`, `price`, `status`) VALUES
	(1, 'Oil change', 45, 'ACTIVE'),
	(2, 'Tyre change', 67, 'ACTIVE'),
	(3, 'Timing belt kit replacement', 174, 'ACTIVE'),
	(4, 'Water pump replacement', 92, 'ACTIVE'),
	(5, 'Fuel filter', 37, 'ACTIVE'),
	(6, 'Cabin air filter', 32, 'ACTIVE'),
	(7, 'Engine air filter', 33, 'ACTIVE'),
	(8, 'Spark plugs change', 27, 'ACTIVE'),
	(9, 'Coolant replacement', 112, 'ACTIVE'),
	(10, 'Brake fluid replacement', 114, 'ACTIVE'),
	(11, 'Brake pad set - Front axle', 48, 'ACTIVE'),
	(12, 'Brake pad set - Rear axle', 43, 'ACTIVE');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table smartgarage.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  PRIMARY KEY (`token_id`),
  KEY `tokens_users_user_id_fk` (`user_id`),
  CONSTRAINT `tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` (`token_id`, `token`, `user_id`, `expiry_date`) VALUES
	(1, '54e14187-7f8d-4114-9c0c-9dd572c7ffd9', 6, '2022-04-15'),
	(2, '2c06e28c-f759-4489-99e7-e17f41909a35', 8, '2022-04-24');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;

-- Dumping structure for table smartgarage.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `password` char(60) NOT NULL,
  `email` varchar(32) DEFAULT NULL,
  `currency` enum('BGN','EUR','USD') NOT NULL DEFAULT 'BGN',
  `enabled` tinyint(1) NOT NULL,
  `join_date` date NOT NULL,
  `last_login_date` date DEFAULT NULL,
  `last_login_date_display` date DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.users: ~8 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `phone`, `password`, `email`, `currency`, `enabled`, `join_date`, `last_login_date`, `last_login_date_display`) VALUES
	(1, 'cdimitrov', 'Chavdar', 'Dimitrov', '0885810921', '$2a$12$mlbxQAP11NEvqwRsI9JtW.JEGe6yluqHqK1NzsUUNq0AiRfpQryLG', 'chavdardim@gmail.com', 'BGN', 1, '2021-04-16', '2022-04-27', '2022-04-27'),
	(2, 'istavrev', 'Ivaylo', 'Stavrev', '0855233421', '$2a$12$mlbxQAP11NEvqwRsI9JtW.JEGe6yluqHqK1NzsUUNq0AiRfpQryLG', 'litosarm@gmail.com', 'BGN', 1, '2013-04-16', '2022-04-28', '2022-04-28'),
	(3, 'v.valchev', 'Victor', 'Valchev', '0888111111', '$2a$12$46AjGUD49c0byd1tq1fP9Onpr3o6mAsgNAIvtY4UuaREVEDR.gXTO', 'victor@abv.bg', 'BGN', 1, '2021-08-12', '2022-04-15', '2022-04-15'),
	(4, 'vlad', 'Vladimir', 'Venkov', '0888222222', '$2a$12$k.Be88ypF8HNaEX3cJwfm.yd2/ZlnYhC/QYHJTDS6G1DeTomSW47S', 'vladi@gmail.com', 'BGN', 1, '2019-06-07', '2022-04-15', '2022-04-15'),
	(5, 'idimiev', 'Ivo', 'Dimiev', '0899333333', '$2a$12$yXmrA7gZEyiaRKffkm9//OXcC4TlKSFnxPphAHBNREpGxT1JLKAYa', 'ivo_dimiev@telerik.com', 'BGN', 1, '2017-10-13', '2022-04-15', '2022-04-15'),
	(6, 'slow-carb', 'Johnie', 'McStone', '0888334455', '$2a$11$W2O2iuWxjs39dhUFkdlh0.6mzMT7NC41hX.4eTyCcKdl8QX9S.iaO', 'test@test111.com', 'BGN', 1, '2019-04-18', '2022-04-15', '2022-04-15'),
	(8, 'mumblecore', 'Georgi', 'Nikolov', '0854222111', '$2a$11$yZ7RTzC1yTmd5zHzpPrReuEcXedIlzEihuLWy/N.VNex7vssm.oEO', 'test@test.com', 'BGN', 0, '2022-04-23', NULL, NULL),
	(9, 'theFather', 'Vito', 'Corleone', '5627609418', '$2a$12$mlbxQAP11NEvqwRsI9JtW.JEGe6yluqHqK1NzsUUNq0AiRfpQryLG', 'vito@mafioso.italianno', 'BGN', 1, '2022-04-20', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table smartgarage.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  KEY `users_roles_roles_role_id_fk` (`role_id`),
  KEY `users_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.users_roles: ~8 rows (approximately)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
	(1, 2),
	(2, 2),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(8, 1),
	(9, 1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits
CREATE TABLE IF NOT EXISTS `visits` (
  `visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `check_in` datetime NOT NULL,
  `check_out` datetime DEFAULT NULL,
  `car_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`visit_id`),
  KEY `visits_cars_car_id_fk` (`car_id`),
  KEY `visits_users_user_id_fk` (`user_id`),
  CONSTRAINT `visits_cars_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
  CONSTRAINT `visits_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.visits: ~9 rows (approximately)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, `check_in`, `check_out`, `car_id`, `user_id`) VALUES
	(4, '2022-04-13 11:53:34', '2022-04-13 11:54:02', 2, 2),
	(10, '2022-04-13 17:18:25', '2022-04-13 17:19:21', 2, 2),
	(11, '2022-04-13 17:25:17', '2022-04-13 18:26:36', 1, 1),
	(13, '2022-04-15 11:47:27', '2022-04-22 13:33:27', 7, 2),
	(15, '2022-04-19 14:27:27', NULL, 4, 3),
	(18, '2022-04-23 20:43:28', NULL, 5, 4),
	(19, '2022-04-23 22:29:50', NULL, 6, 5),
	(20, '2022-04-24 13:45:11', '2022-04-24 13:46:10', 19, 6),
	(21, '2022-04-27 15:36:17', '2022-04-27 15:37:26', 20, 8);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;

-- Dumping structure for table smartgarage.visits_services
CREATE TABLE IF NOT EXISTS `visits_services` (
  `visit_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  KEY `cars_services_services_service_id_fk` (`service_id`),
  KEY `visits_services_visits_visit_id_fk` (`visit_id`),
  CONSTRAINT `cars_services_services_service_id_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
  CONSTRAINT `visits_services_visits_visit_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table smartgarage.visits_services: ~19 rows (approximately)
/*!40000 ALTER TABLE `visits_services` DISABLE KEYS */;
INSERT INTO `visits_services` (`visit_id`, `service_id`) VALUES
	(4, 3),
	(4, 2),
	(4, 1),
	(10, 1),
	(10, 6),
	(11, 2),
	(11, 3),
	(11, 1),
	(11, 9),
	(13, 1),
	(13, 5),
	(13, 7),
	(20, 1),
	(20, 8),
	(20, 10),
	(19, 3),
	(21, 5),
	(21, 1),
	(18, 4);
/*!40000 ALTER TABLE `visits_services` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
